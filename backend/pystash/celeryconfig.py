"""
PyStash Release Manager REST URL-based JSON API

Celery configuration file.

See http://docs.celeryproject.org/en/latest/getting-started/first-steps-with-celery.html#configuration
to understand the settings below.
"""


__author__ = 'llentini'

import djcelery
djcelery.setup_loader()

# List of modules to import when celery starts.
# currently useless as auto discovery is enabled.
#CELERY_IMPORTS = ('pystash.tasks', )

BROKER_URL = "django://"  # tell kombu to use the Django database as the message queue
#TODO: trying to use mongoDB as celery result backend also, but it does not work
# CELERY_RESULT_BACKEND = "django://"
# CELERY_RESULT_BACKEND = 'djcelery.backends.database:DatabaseBackend',

CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_ACCEPT_CONTENT = ['json']

from datetime import timedelta

CELERYBEAT_SCHEDULE = {
    'add-every-60-seconds': {
        'task': 'pystash.tasks.get_repos',
        'schedule': timedelta(seconds=60),
    },
}