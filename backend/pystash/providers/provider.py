"""
This module contains an implementation of an external PyStash data provider.

The aim is to keep requests separated from the logic that process them.
If a new data sources should be plugged in PyStash, the methods here defined could be easily overwritten to
customize the behaviour.

The current implementation is based on Stash REST API.
"""

__author__ = 'llentini'

import re
import logging

# TODO: remove this import when auth will be properly added to each view
# it is only done to get ME currently
from django.conf import settings

from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User

from pystash.providers.stash import StashAPI
from release import models

# A logger instance
logger = logging.getLogger('pystash.release.api')


class ExternalDataProvider():
    """
    Wrapper class to hide the implementation of HTTP requests able to retrieve PyStash relevant data,
    like Stash git repositories, tags and commits inside them, and so on.

    This way, data requests are separated from the logic that process them.
    """
    # the default will be the a StashAPI object targeting our Stash server
    def __init__(self, source=None):
        self.source = source or StashAPI()

    def get_repos(self):
        """
        Utility method to get all available repositories from the SCM tool.
        """
        ME = settings.ME
        # 1/ call Stash REST API to get a list of public repos
        # (cause this is an anonymous request for the moment)
        repos = self.source.get_repos(ME)

        # 2/ properly save them in PyStash DB
        # TODO: saving Stash project as a standard py dict for the moment. Is a proper representation/encoder needed?
        repos_light = [{'remote_id': repo['id'],
                        'name': repo['name'],
                        'remote_url': repo['cloneUrl'],
                        'project': repo['project']
                        } for repo in repos]
        for repo in repos_light:
            # this is not properly working, it creates duplicates!
#                 (_repo, _created)= models.Repository.objects.get_or_create(**(repo))
            try:
                # 'remote_id' should be a unique parameter taken from Stash DB to identify a repo.
                models.Repository.objects.get(remote_id=repo['remote_id'])
                logger.debug("Nothing to create. Repo already in DB")
            except ObjectDoesNotExist, _exc:
                logger.debug("Creating repo in DB")
                user = None
                if repo['project']['isPersonal']:   # there is an owner in the returned JSON
                    # encode the user as repository's owner
                    # TODO: saving/using username only for the moment
                    user, _created = User.objects.get_or_create(
                        username=repo['project']['owner']['name'])
                models.Repository.objects.create(
                    owner=user,
                    remote_id=repo['remote_id'],
                    name=repo['name'],
                    remote_url=repo['remote_url'],
                    project_key=repo['project']['key'])

    def get_releases(self, repo):
        """
        Utility method to parse SCM tags in a specified repository.
        Tags are saved as releases in PyStash.
        """
        ME = settings.ME
        if repo.release_pattern:
            # tmp variable
            releases = repo.releases[:] if repo.releases else []

            # call Stash REST API
            # TODO: a cycle will be maybe needed to handle pagination in Stash REST API.
            tags = self.source.get_repo_tags(repo, ME)
            if tags['size']:
                # tags found
                tags['values'].reverse()
                for tag in tags['values']:  # a python list, ordered from the oldest one to the latest one
                    # check if this tag is already imported
                    if tag['displayId'] not in [release.version for release in releases]:
                        # NOTE: I have manually added a release_pattern in my test repo
                        # http://rndwww.nce.amadeus.net/git-staging/projects/SAN/repos/release-manager-sample/browse
                        # using django shell and specifying the raw string or a normal string
                        # to be tested using django-admin forms.
                        if re.match(repo.release_pattern, tag['displayId']):  # pattern matching
                            # filter the commits, taking the range between this tag
                            # and the latest available in PyStash
                            latest_release = releases[-1] if releases else None
                            if latest_release:
                                # at least a release is already in PyStash
                                # get the commits in the range
                                latest_tag_cset = latest_release.commit
                                commits = self.source.get_commits(repo, ME, since=latest_tag_cset,
                                                                  until=tag['latestChangeset'])['values']
                            else:
                                # no release in PyStash for this repository, get all commits up to the current tag
                                commits = self.source.get_commits(repo, ME, until=tag['latestChangeset'])['values']

                            # Win@ records handling
                            # get all records in the filtered commits
                            records = [commit['attributes']['records'] for commit in commits
                                       if commit.get('attributes')]

                            recs = [item for internal_list in records for item in internal_list]
                            # using get_or_create generates duplicates!
                            release = models.Release.objects.create(version=tag['displayId'],
                                                                    commit=tag['latestChangeset'], remote_id=tag['id'],
                                                                    records=recs)
                            releases.append(release)
        # next comparison is properly done because releases is copy of repo.releases, not a reference!
            if releases != repo.releases:
                repo.releases = releases
                repo.save()