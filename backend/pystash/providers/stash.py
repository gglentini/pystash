__author__ = 'llentini'

from urlparse import urljoin, urlunparse

import requests


class StashAPI():
    """
    Wrapper class to hide the implementation of HTTP request to Stash REST APIs.

    Currently Stash staging server is the only available target.
    """
    # Example:
    # http://rndwww.nce.amadeus.net/git-staging/rest/api/1.0/repos?name=&projectname=&permission=&visibility=&representation=application%2Fjson

    stash_host = "rndwww.nce.amadeus.net"
    stash_path = "/git-staging"
    stash_api_url = "/rest/api/1.0/"

    def __init__(self, host=None, port=None, path=None, **kwargs):
        if host and port:
            netloc = host + ":" + str(port)
        elif host:
            netloc = host
        else:
            netloc = StashAPI.stash_host
        if path:
            stash_path = path
        else:
            stash_path = StashAPI.stash_path + StashAPI.stash_api_url

        parts = ('http', netloc, stash_path, '', '', '')
        # build url from parameters or from default values
        self.url = urlunparse(parts)

        # For anonymous requests to Stash API it is recommended to not specify the permission parameter at all.
#         self.params = kwargs.get('params', {})
#         self.headers = kwargs.get('headers', {'representation': 'application/json'})
#         self.data = kwargs.get('data')

    def get(self, resource="", **kwargs):
        response = requests.get(urljoin(self.url, resource), **kwargs)
        if response.status_code == requests.codes.ok:
            return response
        else:
            response.raise_for_status()

    def post(self, resource="", **kwargs):
        response = requests.post(urljoin(self.url, resource), **kwargs)
        if response.status_code == requests.codes.ok:
            return response
        else:
            response.raise_for_status()

    def get_repos(self, user):
        # url: /repos/
        response = self.get(resource="repos", auth=user)
        return response.json()['values']   # a list of dict

    def get_repo_tags(self, repo, user):
        # url: /projects/{projectKey}/repos/{repositorySlug}/tags/
        response = self.get(resource='projects/'+repo.project_key+'/repos/' + repo.name+'/tags/', auth=user)
        return response.json()

    def get_commit(self, repo, cset_id, user):
        # url:  /projects/{projectKey}/repos/{repositorySlug}/commits/{changesetId:.*}
        response = self.get(resource='projects/'+repo.project_key+'/repos/' + repo.name+'/commits/'+cset_id, auth=user)
        return response.json()

    def get_commits(self, repo, user, since=None, until=None):
        # url: projects/{projectKey}/repos/{repositorySlug}/commits?since=f4ef477&until=34f55d5
        url = 'projects/'+repo.project_key+'/repos/'+repo.name+'/commits/'
        if since and until:
            url += '?since='+since+'&until='+until
        elif since:
            url += '?since='+since
        elif until:
            url += '?until='+until

        response = self.get(resource=url, auth=user)
        return response.json()