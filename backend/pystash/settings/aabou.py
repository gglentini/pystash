"""
Created on 29 Jul 2013

@author: aabou
"""

from .local import *

# Stash account settings
#me = ('my_windows_username', 'my_windows_password')

DATABASES = {
    'default' : {
        'ENGINE' : 'django_mongodb_engine',
        'NAME' : 'pystash_aabou'
    }
}

#INSTALLED_APPS += ('release',
#                   )
