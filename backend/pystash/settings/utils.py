__author__ = 'llentini'

from django.utils import importlib


def import_from_string(val, setting_name):
    """
    Attempt to import a class from a string representation.

    Kindly provided by rest_framework.settings module.
    """
    try:
        # Nod to tastypie's use of importlib.
        parts = val.split('.')
        module_path, class_name = '.'.join(parts[:-1]), parts[-1]
        module = importlib.import_module(module_path)
        return getattr(module, class_name)
    except ImportError as e:
        msg = "Could not import '%s' for API setting '%s'. %s: %s." % (val, setting_name, e.__class__.__name__, e)
        raise ImportError(msg)