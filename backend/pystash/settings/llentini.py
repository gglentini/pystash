"""
Created on 26 Jul 2013

@author: llentini
"""

from .local import *


DATABASES = {
    'default': {
        'ENGINE': 'django_mongodb_engine',
        'NAME': 'pystash_llentini'
    }
}