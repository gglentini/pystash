"""
Local settings file

This is the settings file to be used when you are working on the project locally, and could be used as a template
to create single settings file for each developer to test their own experimental configs.

Local development-specific settings include DEBUG mode, log level,
and activation of developer tools like django-debug-toolbar.
"""


from .base import *

DEBUG = True
TEMPLATE_DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': '',                      # Or path to database file if using sqlite3.
        # The following settings are not used with sqlite3:
        'USER': '',
        'PASSWORD': '',
        'HOST': '',         # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '',                      # Set to empty string for default.
    }
}

LOGGING['loggers']['pystash.release.api']['level'] = 'INFO'

# TEST SETTINGS
TEST_RUNNER = "discover_runner.DiscoverRunner" 
TEST_DISCOVER_TOP_LEVEL = PROJECT_ROOT 
TEST_DISCOVER_ROOT = PROJECT_ROOT 
TEST_DISCOVER_PATTERN = "test_*"


# django-debug toolbar activation and settings
def custom_show_toolbar(request):
    return True  # Always show toolbar in development configuration.

DEBUG_TOOLBAR_CONFIG = {
    'SHOW_TOOLBAR_CALLBACK': custom_show_toolbar,
}

DEBUG_TOOLBAR_PANELS = (
    'debug_toolbar.panels.version.VersionDebugPanel',
    'debug_toolbar.panels.timer.TimerDebugPanel',
    'debug_toolbar.panels.settings_vars.SettingsVarsDebugPanel',
    'debug_toolbar.panels.headers.HeaderDebugPanel',
    'debug_toolbar.panels.request_vars.RequestVarsDebugPanel',
    'debug_toolbar.panels.template.TemplateDebugPanel',
    'debug_toolbar_mongo.panel.MongoDebugPanel',
    'debug_toolbar.panels.signals.SignalDebugPanel',
    'debug_toolbar.panels.logger.LoggingPanel',
)

MIDDLEWARE_CLASSES += (
    # ...
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    # ...
)

INTERNAL_IPS = ('172.16.109.193',)  # TODO: my dev machine, to be properly extended

INSTALLED_APPS += (
    # ...
    'debug_toolbar',
    'debug_toolbar_mongo',
    'djcelery',
    'kombu.transport.django',
)
