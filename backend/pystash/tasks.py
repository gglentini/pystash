"""
PyStash Release Manager REST URL-based JSON API

This module contains Celery tasks definitions.
All asynchronous calls and cron jobs identified in the application should be defined here,
to be then queued by Celery.
"""


from __future__ import absolute_import

__author__ = 'llentini'


import os

from celery import Celery

from django.conf import settings


# set the default Django settings module for the 'celery' program.
# TODO: hardcoded settings file here
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'pystash.settings.llentini')

from pystash.providers.provider import ExternalDataProvider

app = Celery('tasks')

# loading celery dedicated config file
app.config_from_object('pystash.celeryconfig')

# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)


@app.task
def get_repos():
    """
    Retrieve source code repositories from the defined external provider (Stash staging is the current default).

    To be set as a period task in Celery to make this call asynchronous and speed up PyStash page loading.
    """
    source = ExternalDataProvider()
    return source.get_repos()

@app.task
def get_releases():
    """
    Retrieve source code releases (SCM tags in a repository)
    from the defined external provider (Stash staging is the current default).

    To be set as a period task in Celery to make this call asynchronous and speed up PyStash page loading.
    """
    # TODO: find a way to pass the 'repo' paramater here, or refactor the method
    # source = ExternalDataProvider()
    # return source.get_releases(repo)
    pass