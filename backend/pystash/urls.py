from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'pystash.views.home', name='home'),
    # url(r'^pystash/', include('pystash.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    url(r'^release_manager/', include('release.urls')),
    # shared API url, currently pointing to the implementation in release app.
    # TODO: move the implementation to a shared api package
    url(r'^api/', include('release.api.v1.urls')),
    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
