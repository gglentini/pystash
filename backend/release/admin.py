'''
Created on 26 Jul 2013

@author: llentini
'''


from django.contrib import admin

from release.models import Repository, Release


admin.site.register(Repository)
admin.site.register(Release)