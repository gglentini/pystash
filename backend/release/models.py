from django.db import models
from django.contrib.auth.models import User

from djangotoolbox.fields import EmbeddedModelField, ListField, DictField
from django_mongodb_engine.contrib import MongoDBManager

from .forms import ObjectListField, StringListField


class EmbedOverrideField(EmbeddedModelField):
    """
    Overridden class to make django-mongodb-engine field work with django admin forms
    See https://gist.github.com/ielshareef/3011156
    """
    def formfield(self, **kwargs):
        return models.Field.formfield(self, ObjectListField, **kwargs)


class ReleaseListField(StringListField):
    """
    Overridden class to make django-mongodb-engine field work with django admin forms
    See https://gist.github.com/jonashaag/1200165
    """
    def formfield(self, **kwargs):
        return models.Field.formfield(self, StringListField, **kwargs)


class Release(models.Model):
    """
    Representation of a software release.
    """
    class Meta:
        # TODO: trying to NOT have a dedicated collection in mongoDB, still not working.
        managed = False

    version = models.CharField(max_length=20, help_text="Numeric ID of the release.")
    name = models.CharField(max_length=100, help_text="Nickname of the release.")
    remote_id = models.CharField(max_length=100)
    commit = models.CharField(max_length=40)
    records = ListField()

    def __unicode__(self):
        return "%s (%s)" % (self.version, self.name)

class Repository(models.Model):
    """
    Representation of a source code repository.
    """
    # NOTE: URLField currently is validated in the admin form with a regexp
    # not handling git repositories! Everything is fine adding repositories using django shell...
    remote_url = models.URLField(
                help_text="Unique URL, that is used to clone the repository")
    name = models.CharField(max_length=100,)
    release_pattern = models.CharField(max_length=100,
                default="\\d.\\d.\\d", # TODO: remove the default if not necessary
                help_text="The pattern (regexp) for the release tags attached"\
                          " to the repository.")
    # TODO: check if it is working with admin forms...
    owner = EmbedOverrideField(User, null=True)
    # no need (yet) to save Stash project in a dedicated PyStash model
    # but the information is needed to call Stash API
    project_key = models.CharField(max_length=100)
    remote_id = models.IntegerField()
    releases = ListField(EmbeddedModelField(Release), default=[], null=True)
    objects = MongoDBManager()

    def __unicode__(self):
        return "%s - %s" % (self.name, self.remote_url)


