"""
Created on 25 Jul 2013

@author: llentini

PyStash Release Manager REST URL-based JSON API

This module contains the views for Pystash Release Manager, so all the available actions to be done
on any 'object' inside the Release Manager application.
"""

import re
import logging

from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import HttpResponseRedirect

from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.decorators import api_view, link

from . import serializers as release_serializers
from . import exceptions as release_exceptions
from release import models
from pystash.settings.utils import import_from_string

# TODO: remove this import when auth will be properly added to each view
# it is only done to get ME currently
from django.conf import settings

# A logger instance
logger = logging.getLogger('pystash.release.api')
# Pagination default size
PAGINATE_BY = 5


###################################
# shared viewsets, they should be moved to a shared 'pystash.api' package maybe...

@api_view(('GET',))
def api_root(request, format=None, **kwargs):
    return Response({
        'users': reverse('user-list', request=request, format=format),
        'all_visible_repos': reverse('repository-full-list', request=request, format=format, **kwargs),
        #'releases': reverse('release-full-list', request=request, format=format, **kwargs),
    })


class UserViewSet(viewsets.ViewSet):
    """
    RESTful readonly API to list all the users, or get a specific one.
    """

    def list(self, request, format=None):
        queryset = User.objects.all()
        paginator = Paginator(queryset, PAGINATE_BY)
        page = request.QUERY_PARAMS.get('page')
        try:
            users = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer or none, deliver first page.
            users = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999),
            # deliver last page of results.
            users = paginator.page(paginator.num_pages)

        # NOTE: context added after some DeprecationWarning received from rest_framework.
        serializer = release_serializers.PaginatedUserSerializer(users, context={'request': request})
        return Response(serializer.data)

    def retrieve(self, request, pk=None, format=None):
        user = User.objects.get(id=pk)
        # explicitly added many=False in the next call as a workaround for this bug:
        # https://github.com/tomchristie/django-rest-framework/issues/564
        serializer = release_serializers.UserSerializer(user, many=False, context={'request': request})
        return Response(serializer.data)


class RepositoryViewset(viewsets.ViewSet):
    """
    Repos endpoints for all standard actions
    """
    # create an instance of the provider data class
    provider_class = import_from_string(settings.PYSTASH['DATA_PROVIDER'], 'DATA_PROVIDER')
    server = provider_class()

    def list(self, request, format=None, **kwargs):
        """
        GET all available Repository objects.
        """
        # NOTE: it always calls Stash REST API to check if new repositories are available.
        # check this for performances issues, currently it takes ~5 seconds to make this request
        # try:
        #     # get repositories from the external source
        #     self.server.get_repos()
        # except Exception, e:
        #     logger.exception("%s", e)
        #     raise release_exceptions.ExternalProviderException(e.response.status_code, e.response.reason)

        # show repositories list
        queryset = models.Repository.objects.all()
        paginator = Paginator(queryset, PAGINATE_BY)

        page = request.QUERY_PARAMS.get('page')
        try:
            repos = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer or none, deliver first page.
            repos = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999),
            # deliver last page of results.
            repos = paginator.page(paginator.num_pages)

        serializer = release_serializers.PaginatedRepositoryShortSerializer(
            repos,
            # NOTE: context added after some DeprecationWarning received from rest_framework.
            context={'request': request})
        return Response(serializer.data)

    def retrieve(self, request, project_key=None, repo_name=None, format=None, **kwargs):
        """
        GET a single Repository object.

        Currently Stash REST API is also called to retrieving the releases from the SCM tool (Stash+git in this case).
        """
        try:
            repo = models.Repository.objects.get(project_key=project_key, name=repo_name)

            # retrieve tags and Win@proach records from SCM tool, saving them as releases
            try:
                self.server.get_releases(repo)
            except Exception, e:
                raise release_exceptions.ExternalProviderException(e.response.status_code, e.response.reason)

            # explicitly added many=False in the next call as a workaround for this bug:
            # https://github.com/tomchristie/django-rest-framework/issues/564
            serializer = release_serializers.RepositorySerializer(repo, many=False, context={'request': request})
            return Response(serializer.data)
        except ObjectDoesNotExist, e:
            logger.warning("%s", e)
            raise release_exceptions.RepoNotFound()
        except Exception, e:
            logger.error("%s", e)

#     def update(self, request, pk=None, format=None, **kwargs):
#         # TODO: to be completed/tested!!!
#         import pdb; pdb.set_trace()
#         repo = models.Repository.objects.get(id=pk)
#         # update the object, save and return it back
#         serializer = release_serializers.RepositorySerializer(repo, data=request.DATA)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @link()
    def releases(self, request, project_key=None, repo_name=None, format=None):
        """
        Get repository's list of releases.
        """
        try:
            repo = models.Repository.objects.get(project_key=project_key, name=repo_name)
            releases_json = release_serializers.ReleaseSerializer(repo.releases, many=True,
                                                                  context={'request': request})
            return Response(releases_json.data)
        except ObjectDoesNotExist, e:
            logger.warning("%s", e)
            raise release_exceptions.RepoNotFound()


# TODO: does this really worth a Viewset?
class StashRepositoryViewset(viewsets.ViewSet):
    """
    Viewset-based views to feed (create) PyStash repositories using the ones available in Stash.

    """

    def list(self, request):
        """
        Feeds PyStash database with repositories available in Stash.

        All Stash public repositories are currently read, because the request is anonymous.
        """
        try:
            ME = settings.ME
            # 1/ call Stash REST API to get a list of available public repos
            # (cause this is an anounymous request for the moment)
            # TODO: a cycle will be maybe needed to handle pagination in Stash REST API.
            from pystash.providers.provider import ExternalDataProvider
            server = ExternalDataProvider()
            repos = server.get_repos(ME)

            # 2/ properly save them in PyStash DB
            # TODO: saving the Stash project as a standard py dict for the moment
            # is a proper representation/encoder needed?
            repos_light = [{'remote_id':repo['id'], 'name':repo['name'], 'remote_url':repo['cloneUrl'],
                            'project':repo['project']} for repo in repos]
            for repo in repos_light:
                # this is not properly working, it creates duplicates!
#                 (_repo, _created)= models.Repository.objects.get_or_create(**(repo))
                try:
                    # 'remote_id' should be a unique parameter taken from Stash DB to identify a repo.
                    models.Repository.objects.get(remote_id=repo['remote_id'])
                except ObjectDoesNotExist, _exc:
                    if repo['project']['isPersonal']:   # there is an owner in the returned JSON
                        # encode the user as repo's owner
                        # TODO: saving/using username only for the moment
                        user, _created = User.objects.get_or_create(username=repo['project']['owner']['name'])
                        models.Repository.objects.create(owner=user, remote_id=repo['remote_id'], name=repo['name'],
                                                         remote_url=repo['remote_url'], project=repo['project'])
                    else:
                        models.Repository.objects.create(**repo)

            # show repositories list after import
            return HttpResponseRedirect(reverse('repository-list'))
        except Exception, _e:
            raise

class StashRepositoryTagViewset(viewsets.ViewSet):
    """
    Viewset-based views to feed (create) PyStash repositories' tags using the ones available in Stash.

    A tag associated to a particular commit identifies a release, thus Release PyStash table will be feed.

    """

    def list(self, request):
        """
        Feeds PyStash repositories with relative tags available in Stash.

        The request must be done by an authenticated user, and he must have REPO_READ permission
        for the context repository to call the tag(s).
        """
        try:
            ME = settings.ME
            from pystash.providers.provider import ExternalDataProvider
            server = ExternalDataProvider()
            for repo in models.Repository.objects.all():
                if repo.release_pattern:
                    # tmp variable
                    releases = repo.releases[:] if repo.releases else []

                    # call Stash REST API
                    # TODO: a cycle will be maybe needed to handle pagination in Stash REST API.
                    tags = server.get_repo_tags(repo, ME)
                    try:
                        if tags['size']:
                            # tags found
                            tags['values'].reverse()
                            for tag in tags['values']:  # a python list, ordered from the oldest one to the latest one
                                # check if this tag is already imported
                                if tag['displayId'] not in [release.version for release in releases]:
                                    # NOTE: I have manually added a release_pattern in my test repo
                                    # http://rndwww.nce.amadeus.net/git-staging/projects/SAN/repos/release-manager-sample/browse
                                    # using django shell and specifying the raw string or a normal string
                                    # repo.release_pattern = r"\d.\d.\d"
                                    # repo.release_pattern = "\d.\d.\d"
                                    # to be tested using django-admin forms.
                                    if re.match(repo.release_pattern, tag['displayId']):  # pattern matching
                                        # Win@ records handling
                                        # 1/ get (call Stash API) all records between the commit associated to this tag
                                        # and the one linked to the latest imported release

                                        # NOTE: using /projects/{projectKey}/repos/{repositorySlug}/commits?since=<id>&until=<id>
                                        # DOES NOT send back 'attributes' field for each changeset,
                                        # that is where we have stored Win@ records IDs...
                                        # for now, get ALL commits until the one associated to the current tag...

                                        commits_until_curr_tag = server.get_commits(repo, ME,
                                                                                    until=tag['latestChangeset'])['values']
                                        commits = []

                                        # 2/ filter the commits, taking the range between this tag
                                        # and the latest available in PyStash
                                        latest_release = releases[-1] if releases else None
                                        if latest_release:
                                            # at least a release is already in PyStash
                                            # 2.1/ take the changeset associated to the latest release available
                                            latest_tag_cset = latest_release.commit
                                            # NOTE: Stash REST API bug found!
                                            # 'since' parameter in the request is IGNORED, ALL commits are returned!!!
                                            # commits_since_latest_tag = server.get_commits(repo, ME, since=latest_tag_cset)
                                            # 2.2/ slice the list of commit to get the relevant one(s)
                                            for commit in commits_until_curr_tag:
                                                if commit['id'] == latest_tag_cset:
                                                    break
                                                else:
                                                    commits.append(commit)

                                        records = [commit['attributes']['records'] for commit in commits
                                                   if commit.get('attributes')]
                                        # TODO: flattening a list of lists...maybe something better here?
                                        recs = [item for internal_list in records for item in internal_list]
                                        # using get_or_create generates duplicates!
                                        release = models.Release.objects.create(version=tag['displayId'],
                                                                                commit=tag['latestChangeset'],
                                                                                remote_id=tag['id'], records=recs)
                                        releases.append(release)
                    except KeyError, e:
                            logger.error("%s", e)
                    # next comparison is properly done because releases is copy of repo.releases, not a reference!
                    if releases != repo.releases:
                        repo.releases = releases
                        repo.save()
                # a view must always return a Response or HttpResponse
            return HttpResponseRedirect(reverse('release-full-list'))
        except Exception, _e:
            raise

#############################################


class ReleaseViewset(viewsets.ViewSet):
    """
    RESTful readonly API to list all the releases, or get a specific one.
    """

    def list(self, request, format=None):
        queryset = models.Release.objects.all()
        # NOTE: context added after some DeprecationWarning received from rest_framework.
        serializer = release_serializers.ReleaseSerializer(queryset, many=True, context={'request': request})
        return Response(serializer.data)

    def retrieve(self, request, project_key=None, repo_name=None, version=None, format=None):
        try:
            repo = models.Repository.objects.get(project_key=project_key, name=repo_name)
            releases = repo.releases

            # non pythonic implementation, more optimized
            release = None
            for inner_release in releases:
                if inner_release.version == version:
                    release = inner_release
                    break
            # pythonic implementation, but iterates through all releases
#             release = [inner_release for inner_release in releases if inner_release.version == version]
            if not release:
                logger.warning('Release not found.')
                raise release_exceptions.ReleaseNotFound()
#             else:
#                 release = release[0]

            # explicitly added many=False in the next call as a workaround for this bug:
            # https://github.com/tomchristie/django-rest-framework/issues/564
            serializer = release_serializers.ReleaseSerializer(release, many=False, context={'request': request})
            return Response(serializer.data)
        except ObjectDoesNotExist, e:
            logger.warning("%s", e)
            raise release_exceptions.RepoNotFound()

