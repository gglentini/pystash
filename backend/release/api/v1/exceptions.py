'''
Created on 22 Aug 2013

@author: llentini
'''


from rest_framework import exceptions, status


class RepoNotFound(exceptions.APIException):
    """
    Custom exceptions to be used when a PyStash repo is not found.
    """
    def __init__(self):
        self.status_code = status.HTTP_404_NOT_FOUND
        self.detail = "Repository not found."
        

class ReleaseNotFound(exceptions.APIException):
    """
    Custom exceptions to be used when a PyStash release is not found.
    """    
    def __init__(self):
        self.status_code = status.HTTP_404_NOT_FOUND
        self.detail = "Release not found."


class ExternalProviderException(exceptions.APIException):
    """
    Custom exception to be used to propagate an exception raised from the external data provider
    E.g. an error sent back from Stash
    """
    def __init__(self, status_code, message):
        self.status_code = status_code
        self.detail = message