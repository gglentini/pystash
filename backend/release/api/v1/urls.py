"""
Created on 25 Jul 2013

@author: llentini

PyStash Release Manager REST API URLconf
"""

from django.conf.urls import patterns, url

from rest_framework.urlpatterns import format_suffix_patterns

from . import views


urlpatterns = patterns('',
    url(r'^$', 'release.api.v1.views.api_root'),

    url(r'stash_repos/?$', views.StashRepositoryViewset.as_view({'get': 'list'}), name='import-stash-repos'),

    url(r'stash_repos_tags/?$', views.StashRepositoryTagViewset.as_view({'get': 'list'}), name='import-stash-repos-tags'),

    url(r'users/?$', views.UserViewSet.as_view({'get': 'list'}), name="user-list"),
    url(r'users/(?P<pk>[a-z0-9]+)/?$', views.UserViewSet.as_view({'get': 'retrieve'}), name="user-detail"),

    # url(r'projects/(?P<project_key>[a-zA-Z~]+)/repos/?$', views.RepositoryViewset.as_view({'get': 'list'}),
    #     name="repository-list"),
    url(r'all_visible_repos/?$', views.RepositoryViewset.as_view({'get': 'list'}), name="repository-full-list"),

    # TODO: better regexp for projects keys are repo names, to cover more use cases
    url(r'projects/(?P<project_key>[a-zA-Z~]+)/repos/(?P<repo_name>[a-zA-Z0-9-_ ]+)/?$',
        views.RepositoryViewset.as_view({'get': 'retrieve',}), name="repository-detail"),
    url(r'projects/(?P<project_key>[a-zA-Z~]+)/repos/(?P<repo_name>[a-zA-Z0-9-_ ]+)/releases/?$',
        views.RepositoryViewset.as_view({'get': 'releases'}), name="repository-releases"),

    url(r'^releases/?$', views.ReleaseViewset.as_view({'get': 'list'}), name="release-full-list"),
    url(r'^releases/(?P<pk>[a-z0-9]+)/?$', views.ReleaseViewset.as_view({'get': 'retrieve'}), name="release-full-detail"),
    # TODO: better regexp for release versions
    url(r'projects/(?P<project_key>[a-zA-Z~]+)/repos/(?P<repo_name>[a-zA-Z0-9-_ ]+)/releases/(?P<version>[0-9.]+)/?$',
        views.ReleaseViewset.as_view({'get': 'retrieve'}), name="release-detail"),


)

urlpatterns = format_suffix_patterns(urlpatterns)