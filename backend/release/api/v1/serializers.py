'''
Created on 26 Jul 2013

@author: llentini

PyStash Release Manager REST URL-based JSON API

This module contains the encoders for each Pystash Release Manager 'object' (Django model).

APIv1 is born trying to follow REST standard and guidelines as much as possible:
1/    all the encoders return objects' URLs in order to create a browseable API
2/    all the encoders are using django rest framework utilities to produce a stable, clean, reusable, extendible API

'''

import logging

from django.contrib.auth.models import User

from bson.objectid import ObjectId

from rest_framework import serializers
from rest_framework import pagination
from rest_framework.reverse import reverse

from release.models import Repository, Release #,Project
from . import exceptions


# A logger instance
logger = logging.getLogger('pystash.release.api')


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'id', 'username', 'first_name', 'last_name', 'email')


class PaginatedUserSerializer(pagination.PaginationSerializer):
    """
    Encoder for page objects of user querysets
    """
    class Meta:
        object_serializer_class = UserSerializer


class CustomReleaseURL(serializers.HyperlinkedIdentityField):
    """
    Custom HyperlinkedIdentityField to use multiple lookup fields to get a Release object.
    """
    # taken from http://django-rest-framework.org/api-guide/relations.html#advanced-hyperlinked-fields
    def get_url(self, obj, view_name, request, format):
        # Release object does not have these info by design for the moment...
        # this MongoDB query could be the solution
        # db.release_repository.find({"releases": { $elemMatch: { "id": ObjectId("521b8800abbf4224a5534c90")} } })
        repo_qset = Repository.objects.raw_query( {"releases": { "$elemMatch": { "id": ObjectId(obj.id) } } } )
        if repo_qset:
            repo = repo_qset[0]
        else:
            raise exceptions.RepoNotFound()
        kwargs = {'project_key': repo.project_key, 'repo_name': repo.name, 'version': obj.version}
        return reverse(view_name, kwargs=kwargs, request=request, format=format)

    def get_object(self, queryset, view_name, view_args, view_kwargs):
        version = view_kwargs['version']
        return queryset.get(version=version)


class ReleaseSerializer(serializers.HyperlinkedModelSerializer):
    """
    Encoder for the Release model.
    """
    class Meta:
        model = Release
        fields = ('url','id',) + tuple([f.name for f in model._meta.fields])

    # url pattern : /api/projects/<key>/repos/<repo_name>/releases/<release_version>
    url = CustomReleaseURL(view_name='release-detail', lookup_field='version')


class ReleaseShortSerializer(serializers.ModelSerializer):
    """
    Short encoder for the Release
    """
    class Meta:
        model = Release
        fields = ('url',)

    # url pattern : /api/projects/<key>/repos/<repo_name>/releases/<release_version>
    url = CustomReleaseURL(view_name='release-detail', lookup_field='version')


class CustomRepoURL(serializers.HyperlinkedIdentityField):
    """
    Custom HyperlinkedIdentityField to use multiple lookup fields to get a Repository object.

    Currently builds PyStash repository URL with syntax : /projects/<project_key>/repos/<repo_name>
    """
    def get_url(self, obj, view_name, request, format):
        kwargs = {'project_key': obj.project_key, 'repo_name': obj.name}
        return reverse(view_name, kwargs=kwargs, request=request, format=format)

    def get_object(self, queryset, view_name, view_args, view_kwargs):
        project_key = view_kwargs['project_key']
        repo_name = view_kwargs['repo_name']
        return queryset.get(project_key=project_key, name= repo_name)


class RepositorySerializer(serializers.HyperlinkedModelSerializer):
    """
    Encoder for the Repository model.
    """
    class Meta:
        model = Repository
        fields = ('url', 'id', 'remote_url', 'remote_id', 'name',
                  'project_key', 'owner', 'release_pattern','releases')

    url = CustomRepoURL(view_name='repository-detail', lookup_field='repo_name')
    # using our own encoder here, otherwise I got the 'username' field only...
    owner = UserSerializer()
    releases = ReleaseShortSerializer(many=True)


class RepositoryShortSerializer(serializers.ModelSerializer):
    """
    Short encoder for the Repository

    Currently used in views named 'repository-full-list'
    """
    class Meta:
        model = Repository
        fields = ('url', 'id', 'remote_url', 'remote_id', 'name',
                  'project_key', 'owner', 'release_pattern')

    url = CustomRepoURL(view_name='repository-detail', lookup_field='repo_name')
    owner = UserSerializer()


class PaginatedRepositoryShortSerializer(pagination.PaginationSerializer):
    """
    Short encoder for page objects of repository querysets
    """
    class Meta:
        object_serializer_class = RepositoryShortSerializer
