'''
Created on 28 Aug 2013

@author: llentini

Unit tests for Release Manager REST API

'''

import unittest

from django.test import TestCase
from django.core.urlresolvers import reverse 


class RepositoryViewsetTest(TestCase):


    def setUp(self):
        pass


    def tearDown(self):
        pass


    def test_list(self):
        url = reverse("repository-full-list") 
        response = self.client.get(url) 
        self.assertEquals(response.status_code, 200)

    def test_detail(self):
#         url = reverse("repository-detail") 
#         response = self.client.get(url) 
#         self.assertEquals(response.status_code, 200)
        pass

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()