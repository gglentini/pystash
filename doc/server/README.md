# PyStash
Python app over Stash.

## Specifications

### Document control

### Preface

### Executive summary

### Introduction

PyStash is a [django-REST-framework](http://django-rest-framework.org/)
application which delivers RESTful Web Services over STASH to fill in the gaps
between Platinum and Stash.

### Functional specifications

#### Api Root

```
GET /api/
```

This is the homepage of the REST Api. It is a browsable page that reveals the 1st-level resources.
Clicking on each resource link, we can go down into the tree of resources of the REST Api.

The following are the list of the 1st-level resources and authorized operation on them.

##### Read Repos

```
GET /api/repos/
GET /api/repos/[repo-id]
```

`Repos` stands for repositories.

###### Summary

1. As a user, I want to get the list of repositories hosted on Stash.
2. As a user, I want to read data from a repository hosted on Stash.

###### Examples

Bob is an authenticated user on Stash.<br/>
Doing this operation, Bob gets the list of all repositories that he is authorized to see in Stash.
When he precise the repo-id, he only gets the data for the concerned repository.
Here an example of JSON data get for a repository:

```
GET /api/repos/5200bbe9abbf4221632f6fab/

{
    "url": "http://lnx283:8899/api/repos/5200bbe9abbf4221632f6fab/",
    "id": "5200bbe9abbf4221632f6fab", 
    "name": "glassfs", 
    "remote_url": "http://lnx283/git-staging/scm/~fbauzac/glassfs.git",
    "owner": {
        "url": "http://lnx283:8899/api/users/5200bbe9abbf4221632f6faa/",
        "id": "5200bbe9abbf4221632f6faa", 
        "username": "toto", 
        "first_name": "", 
        "last_name": "", 
        "email": ""
    }, 
    "releases": [], 
    "remote_id": 12, 
    "release_pattern": "\\d.\\d.\\d"
}
```

###### Definitions

A `repos` in PyStash is the resource representing the Stash repository.
A Stash repository is defined by its unique URL (to clone it) and its (internal) unique id, that we called here the `remote_id`.
The `remote_id` is really useful for RESTful queries to the Stash Api.
The listed `repos` are the representation in PyStash of the Stash repositories.
This resource consists in several properties and only the `release_pattern` property is editable by the user.

###### Pre-conditions

* User is an authenticated Stash user

###### Post-conditions

* PyStash database is filled with the new data from Stash repositories.
* If Stash repositories have updated data, they are also updated in PyStash database.

###### Input

1. URL path: `/api/repos/`
**or**
2. URL path with id: `/api/repos/[repo-id]`

###### Output

1. A list of `repos` resources.
**or**
2. One `repos` resource.

And the response status is : OK - HTTP code **200**.

###### Process description

* For URL path: An authenticated user query is made to Stash REST Api to get list of visible repositories by the user.
The data of these repositories are stored in the database of PyStash and an output is sent back to the user.
* For URL path + id: the data of the requested repository, previously stored in PyStash database, are retrieved
and sent back to the user as an output.


#### Read Releases

```
GET /api/repos/[repo-id]/releases
GET /api/repos/[repo-id]/releases/[rel-id]
```

###### Summary

1. As a user, I want to get the list of created releases in a Stash repository.
2. As a user, I want to read data from a specific release in a Stash repository.

###### Examples

Doing this operation, Bob gets the list of all releases created in the Stash repository.
When he precise the rel-id (id of the release), he only gets the data for the concerned release.
Here an example of JSON data get for a release (server running on ncegcolnx283, port 8899):

```
GET /api/repos/5200bbe9abbf4221632f6fab/releases/5200c12eabbf4221632f6fb2/

[
    {
        "id": "5200c12eabbf4221632f6fb2", 
        "version": "0.1.1", 
        "name": "", 
        "remote_id": "refs/tags/0.1.1", 
        "commit": "0bf009c4431d342b93799c99ddacb5b4a0895b17", 
        "wina_records": [
            {
                "number": "5022549",
                "type": "TR"
            },
            {
                "number": "5022349",
                "type": "PTR"
            }
        ], 
        "url": "http://lnx283:8899/api/repos/5200bbe9abbf4221632f6fab/releases/5200c12eabbf4221632f6fb2/"
    }, 
    {
        "id": "5200c12fabbf4221632f6fb3", 
        "version": "0.1.0", 
        "name": "Genesis", 
        "remote_id": "refs/tags/0.1.0", 
        "commit": "5e7b65398760517a76d307966b2090e243f66ea3", 
        "wina_records": [], 
        "url": "http://lnx283:8899/api/repos/5200bbe9abbf4221632f6fab/releases/5200c12fabbf4221632f6fb3/"
    }
]
```

###### Definitions

A `release` in PyStash is the resource representing a Software Release tag created in Stash on a repository.

Generally the Software Release tag follows a pattern that allows to identify easily the version of the Software.
This tag allows to the user to identify the commit as of which his/her changes are taken into account.
The changes made in the source code are obviously tracked by commits and the policy is to add the _Winaproach_ records
that justify these changes directly in the commits message.
Given that an internal Stash plugin was developed to have these _Winaproach_ records returned in a structured manner
by the Stash REST Api, the _Winaproach_ records are also stored in the PyStash database as property of the `release` resource.
Getting a `release` is then the operation to have the list of possible _Winaproach_ records between a requested tag and
the previous descendent-ordered one, following a certain pattern. If there is no previous tag, the list of all possible _Winaproach_
records since the beginning of the repository are returned.

###### Pre-conditions

* User is an authenticated Stash user
* Repos has property release_pattern defined.

###### Post-conditions

* None

###### Input

1. URL path: `/api/repos/[repo-id]/releases`
**or**
2. URL path with id: `/api/repos/[repo-id]/releases/[rel-id]`

###### Output

1. A list of `releases` resources.<br>
**or**
2. One `release` resource.

And the response status is : OK - HTTP code **200**.

###### Process description

* The data already stored in PyStash database are sent to the user as an output.


#### Read Wina_records

```
GET /api/wina_records/
GET /api/wina_records/[record-number]
```

`Wina_records` stands for _Winaproach_ records.

###### Summary

1. As a user, I want to get the list of _Winaproach_ records stored in the PyStash database.
2. As a user, I want to get data from a _Winaproach_ records, so that I can read the list of repositories+releases which include this record.

###### Examples

Bob is an authenticated user on Stash.
Doing this operation, Bob gets the list of all _Winaproach_ records referenced in PyStash releases.
When he precise the _Winaproach_ record number, he gets the data concerning the releases and the repositories which referenced this record.
Here is an example of JSON data got for a _Winaproach_ record (server running on lnx283, port 8899):

```
GET /api/wina_records/5022549

{
    "wina_number": "5022549",
    "type": "TR",
    "url": "http://ncegcolnx283:8899/api/wina_records/5022549",
    "in_repos": [
        {
            "id": "5200bbe9abbf4221632f6fab",
            "url": "http://lnx283:8899/api/repos/5200bbe9abbf4221632f6fab/",
            "name": "glassfs",
            "remote_url": "http://git-staging/scm/~fbauzac/glassfs.git",
            "in_releases": [
                {
                    "id": "5200c12eabbf4221632f6fb2",
                    "version": "0.1.3",
                    "name": "Aposia",
                    "url": "http://lnx283:8899/api/repos/5200bbe9abbf4221632f6fab/releases/5200c12eabbf4221632f6fb2/"
                }
            ]
        }
    ]
}
```

###### Definitions

`wina_records` in PyStash are the resources which permit to get the list of `repos` whose `releases` contain the specified
_Winaproach_ record number.

###### Pre-conditions

* none

###### Post-conditions

* none

###### Input

1. URL path: `/api/wina_records/`
**or**
2. URL path with _Winaproach_ record number: `/api/wina_records/[record-number]`

###### Output

1. A list of `wina_records` resources.
**or**
2. one `wina_record` resource.


###### Process description

* The data already stored in PyStash database are sent to the user as an output.

#### Read Users

###### Summary

###### Examples

###### Definitions

###### Pre-conditions

###### Post-conditions

###### Input

###### Output

###### Process description

###### Errors

### Other specifications

### Glossary

*   *MongoDB*:
    See the [MongoDB](http://docs.mongodb.org/) documentation website.

*   *Release*:
    Currently the term *Release* is representing a certain level of a source code which is determined by a tag
    in a SCM.

*   *SCM*:
    Source Control Management. Generally apply for tools like Git, Mercurial, CVS, etc ...

### Appendices
