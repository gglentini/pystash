# PyStash (release manager)
A Python app over Stash.

## Overview

PyStash is a [django-REST-framework](http://django-rest-framework.org/) project.
The goal is to develop RESTful Web Services above STASH to close the gaps between
Stash and Amadeus internal applications, and ease the usage of developers.

## Documentation and Specifications

Please browse `doc` folder on this page.

## Developers guide

Read this if you intended to contribute on the project.

### Coding rules

The coding convention of this project is based on the standard [PEP 8](http://www.python.org/dev/peps/pep-0008).<br/>
When it can not be applied, the common sense prevails and the rule that is <br/>
mostly used in the project - even different from the standard - **MUST** be applied.

### Installation

#### Requirements

We recommend to use [pip](http://www.pip-installer.org/en/latest/) and 
[virtualenv](http://www.virtualenv.org/) to install the application
(they are standard for Django projects.)

At least `git version 1.8` **SHOULD** be installed on the machine you're using.

PyStash is using [mongodb](http://docs.mongodb.org/), so you **MUST** have an instance up and running.

Go to your usual development folder (e.g. `dev`):

    (PyStash)$> cd ~/dev

and clone the source code into repository `PyStash` from the URL you get when clicking on the `Clone`
button on this page. (Camelcase is used here to avoid confusion).

Then do:

    (PyStash)$> cd PyStash
    (PyStash)$> pip install -r requirements/local.txt

If everything is OK at this point, then your local PyStash is **SUCCESSFULLY** installed!

#### Configuration

PyStash is currently using django-nonrel 1.4, which is a fork of django 1.4 (for NoSQL).<br/>
To overcome a [bug](https://code.djangoproject.com/ticket/16017/) in Django 1.4 (currently fixed only in 1.5 branch), we must follow this [tip](http://stackoverflow.com/a/15641448/)<br/>
and set this environment variable:

    (PyStash)$> export LANG="en_US.UTF-8"

Now create your settings copying `settings/llentini.py` and updating into your `[username].py`.

##### Set your SITE_ID

Your database settings MUST be correctly set at this stage and your mongoDB MUST be up and running.<br/>
Create a site in Django:

    $> python manage.py shell --settings=pystash.settings.[username]
    >>> from django.contrib.sites.models import Site
    >>> s = Site(domain='pystash', name='PyStash')
    >>> s.save()

Then get the site ID:

    $> mongo [your_db]

    MongoDB shell version: 2.2.3
    ...
    > db.django_site.find()
    { "_id" : ObjectId("517bef0f0ed8c7221316f2e3"), "domain" : "pystash", "name" : "PyStash" }
	
	# Here SITE_ID = '517bef0f0ed8c7221316f2e3'

Set `SITE_ID` in your `settings/[username].py` and everything **SHOULD** work!

#### Run the server

Once your settings are up-to-date, do:

    (PyStash)$>python manage.py runserver 0.0.0.0:[port] --settings=pystash.settings.[username]

Your local PyStash is **up and running**. You can start playing with it!

