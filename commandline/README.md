# PyStashCLI
<i>Python app over Stash to fill in the gaps between
 [Platinum](http://platinum) and Stash</i>

## Overview

PyStashCLI is the Command LIne client for PyStash server.<br/>
The goal is to close the gaps between Stash and Amadeus internal applications and ease the usage of developers.

## Documentation and Specifications

Please browse `doc` in the parent folder.

## User guide

You have to execute `pys` in the folder `commandline/pystash/`.

For more details, do `pys -h`

## Developers guide

## Support & Troubleshooting

Contacts:

> [Ange Abou](mailto:aabou@amadeus.com)<br/>
> [Luigi Lentini](mailto:luigi.lentini@amadeus.com)<br/>
> [Chingis Dugarzhapov](mailto:chingis.dugarzhapov@amadeus.com)<br/>