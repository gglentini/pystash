# -*- coding: utf-8 -*-
#
# Copyright (C) 2013 Amadeus SAS
# All rights reserved.

import argparse, logging, os
from sys import stdout as stdout

import requests


DESC="""PyStash Command Line client"""

class PysStyle:
    """"Wrapper class to style the standard output
    """

    @staticmethod
    def init_style(color=True):
        """Style initialisation"""
        PysStyle.STYLE = {
             "default"      : "\033[0m" if color else "",
             # styles
             "bold"         : "\033[01m" if color else "",
             "underline"    : "\033[04m" if color else "",
             "blink"        : "\033[05m" if color else "",
             "reverse"      : "\033[07m" if color else "",
             "concealed"    : "\033[08m" if color else "",
             # font colors
             "black"        : "\033[30m" if color else "",
             "red"          : "\033[31m" if color else "",
             "green"        : "\033[32m" if color else "",
             "yellow"       : "\033[33m" if color else "",
             "blue"         : "\033[34m" if color else "",
             "magenta"      : "\033[35m" if color else "",
             "cyan"         : "\033[36m" if color else "",
             "white"        : "\033[37m" if color else "",
             "grey"         : "\033[0m" if color else "",
             # background colors
             "on_black"     : "\033[40m" if color else "",
             "on_red"       : "\033[41m" if color else "",
             "on_green"     : "\033[42m" if color else "",
             "on_yellow"    : "\033[43m" if color else "",
             "on_blue"      : "\033[44m" if color else "",
             "on_magenta"   : "\033[45m" if color else "",
             "on_cyan"      : "\033[46m" if color else "",
             "on_white"     : "\033[47m" if color else "",
        }

        PysStyle.EOL = PysStyle.STYLE['default'] + os.linesep
        PysStyle.TAB = 4 * " "


class PysDisplay(object):
    """"Handler class in charge of the command output

    Handles the standard output display
    """
    # --- Output line ---
    AND_MORE = " and more..."

    @staticmethod
    def _header_repo(repo):
        """Internal helper method

        Displays the common header for listrepos and showrepo.

        --- Output lines ---
        repo title:                 @ repo_name #i (~owner)
        repo URL :                    Clone from: http://url_to_clone
        repo releases count:          Has x release[s] of pattern: release_pattern

        **Note**: `PysStyle.init_style()` MUST be called first
        """
        PysDisplay.REPOS_TITLE = PysStyle.STYLE['bold'] +\
            PysStyle.STYLE['underline'] + "@ %s%s" + PysStyle.EOL
        PysDisplay.REPOS_OWNER = " (~%s)"
        PysDisplay.REPOS_URL = PysStyle.TAB +"Clone from: "+\
            PysStyle.STYLE['magenta'] + "%s" + PysStyle.EOL
        PysDisplay.REPOS_RELEASE_COUNT = PysStyle.TAB + "Has %d release%s%s" +\
            PysStyle.EOL
        PysDisplay.REPOS_RELEASE_PATTERN = " of pattern: %s"
        PysDisplay.REPOS_RELEASES = PysStyle.TAB + "Releases: %s%s" +\
            PysStyle.EOL


        # repo title
        title_text = "%s #%s" % (repo['name'], repo['remote_id'])
        title = PysDisplay.REPOS_TITLE % (title_text, "")
        if repo['owner']:
            logging.debug("list_repos: repo has owner: %s", repo['owner'])
            owner = PysDisplay.REPOS_OWNER % repo['owner']['username']
            title = PysDisplay.REPOS_TITLE % (title_text, owner)
        stdout.write(title)
        # repo URL
        url = PysDisplay.REPOS_URL % repo['remote_url']
        stdout.write(url)
        # repo releases count
        pattern = PysDisplay.REPOS_RELEASE_PATTERN % \
            repo['release_pattern'] if repo['release_pattern'] else ""
        count = len(repo['releases'])
        releases_count = PysDisplay.REPOS_RELEASE_COUNT % (count, "",
                                                           pattern)
        if count > 1:
            releases_count = PysDisplay.REPOS_RELEASE_COUNT % (
                                             count, "s", pattern)
        stdout.write(releases_count)

    @staticmethod
    def list_repos(repos):
        """Display of the listrepos command.

        --- Output line after Header ---
        repo releases:                Releases: x.y.z, s.t.u, [and more...]
        """
        logging.info("Repos list display")
        # Initialise the style
        PysStyle.init_style()

        for repo in repos:
            # repo header
            PysDisplay._header_repo(repo)
            # repo releases
            repo_releases = repo['releases']
            versions = [
                str(release['version']) for release in repo_releases
                # converted from unicode into string
            ]
            and_more = ""
            if len(versions) > 3:
                versions = versions[:2]
                and_more = PysDisplay.AND_MORE
            versions_text = None
            if versions:
                logging.debug("versions: %s", versions)
                versions = sorted(versions, key=str.lower, reverse=True)
                versions_text = ", ".join(versions)
            if versions_text:
                releases = PysDisplay.REPOS_RELEASES % (versions_text,
                                                        and_more)
                stdout.write(releases)
            stdout.write(PysStyle.EOL)

    @staticmethod
    def _version_repo(releases):
        """Internal helper method

        Displays the common list of releases with their winaproach records.

        --- Output lines ---
        repo release version:            * x.y.z
        repo winaproach record:             PTR 1234567
                                            TR 7654321

        **Note**: `PysStyle.init_style()` MUST be called first
        """
        PysDisplay.REPO_RELEASE_VERSION = 2 * PysStyle.TAB +\
            PysStyle.STYLE['bold'] + "* %s" + PysStyle.EOL
        PysDisplay.REPO_WINA_RECORD = 3 * PysStyle.TAB +\
            PysStyle.STYLE['cyan'] + "%s" + PysStyle.EOL

        for release in releases:
            version = PysDisplay.REPO_RELEASE_VERSION % release['version']
            stdout.write(version)
            # repo winaproach record
            records = release['records']
            for record in records:
                winaproach = PysDisplay.REPO_WINA_RECORD % record
                stdout.write(winaproach)

    @staticmethod
    def show_repo(repo):
        """Display of the showrepo command.

        --- Output lines after Header ---
        repo releases:                Releases:
        repo release version:            * x.y.z
        repo winaproach record:             PTR 1234567
                                            TR 7654321
        """
        logging.info("Repo display")
        # Initialise the style
        PysStyle.init_style()

        # repo header
        PysDisplay._header_repo(repo)
        # repo releases
        repo_releases = repo['releases']
        if repo_releases:
            releases = PysDisplay.REPOS_RELEASES % ("", "")
            stdout.write(releases)
        # repo releases versions
        PysDisplay._version_repo(repo_releases)
        stdout.write(PysStyle.EOL)

    @staticmethod
    def show_repo_release(repo, tag):
        """Display of the showrepo command with option -t

        --- Output lines after Header ---
        repo release:                Release:
        repo release version:            * x.y.z
        repo winaproach record:             PTR 1234567
                                            TR 7654321
        """
        logging.info("Release display")
        # Initialise the style
        PysStyle.init_style()

        PysDisplay.REPO_RELEASE = PysStyle.TAB + "Release:" + PysStyle.EOL

        # repo header
        PysDisplay._header_repo(repo)
        # repo releases
        repo_releases = repo['releases']
        rel_to_show = []
        error_msg = None
        if not repo_releases:
            error_msg = "pys: error: release not found" + os.linesep

        if not error_msg:
            for release in repo_releases:
                if tag == release['version']:
                    rel_to_show.append(release)

            if not rel_to_show:
                    error_msg = "pys: error: release not found" + os.linesep

        if error_msg:
            stdout.write(error_msg)
            return

        # release line
        stdout.write(PysDisplay.REPO_RELEASE)
        # repo release version
        PysDisplay._version_repo(rel_to_show)
        stdout.write(PysStyle.EOL)


class Backend(object):
    """"Wrapper class to call the PyStash Server

    Encapsulates all the requests sent to the server
    """
    _BASE_URL = 'http://localhost:8899/api/'

    def __init__(self, uri, **kwargs):
        self.url = Backend._BASE_URL + uri

    def get(self, **kwargs):
        response = requests.get(self.url, **kwargs)
        return response.json()

    def post(self, **kwargs):
        response = requests.post(self.url, **kwargs)
        return response.json()


def listrepos(args):
    """"Command listrepos

    Displays the list of Stash repositories with their relevant info.
    """
    logging.info("Starting listrepos")
    Backend('repos').get()
    # FIXME: Intermediate call to have the tags updated in repos
    Backend('stash_repos_tags').get()
    repos = Backend('repos').get()
    #repo_names = [repo['name'] for repo in repos]
    PysDisplay.list_repos(repos)


def showrepo(args):
    """Command showrepo

    Displays a single Stash repository relevant info.
    """
    logging.info("Starting showrepo")
    repo_id = args.repo_id
    repos = Backend('repos').get()
    # Find repo with repo_id
    Found = False
    for repo in repos:
        if repo_id == repo['remote_id']:
            if args.release_tag:
                PysDisplay.show_repo_release(repo, args.release_tag)
            else:
                PysDisplay.show_repo(repo)
            Found = True
            break
    # Error message if not found
    if not Found:
        error_msg = "pys: error: repo not found" + os.linesep
        stdout.write(error_msg)


if __name__ == '__main__':
    # Entry point

    # Top-level parser
    parser = argparse.ArgumentParser(prog="pys", description=DESC)
    parser.add_argument("--verbose", type=int, dest="verbose",
        default=0,
        help="set logging level  "\
            "0: error (default), 1: error+warning, 2: error+warning+info, "\
            "3: error+warning+info+debug.")
    subparsers = parser.add_subparsers(help="available commands")
    # Parser for listrepos command
    parser_lr = subparsers.add_parser('listrepos',
        help='displays the list of Stash repositories')
    parser_lr.set_defaults(func=listrepos)
    # Parser for showrepo command
    parser_sr = subparsers.add_parser('showrepo',
        help='displays the info of the Stash repository')
    parser_sr.add_argument('-r', '--repo', type=int, dest='repo_id',
        required=True, help='the repository id (#number)')
    parser_sr.add_argument('-t', '--tag', dest='release_tag',
        help='the tag corresponding to the release')
    parser_sr.set_defaults(func=showrepo)
    # command TODO:
    # listreleases, showrelease, ...
    args = parser.parse_args()

    log_level = {
        '0': logging.ERROR,
        '1': logging.WARNING,
        '2': logging.INFO,
        '3': logging.DEBUG
    }

    if args.verbose:
        logging.basicConfig(level=log_level[str(args.verbose)])

    # Call the right default function
    args.func(args)